---
_build: { list: never }
title: About Me
comments: false
bigimg: [ src: img/johannes-schenk-8mj7KKvyMDk-unsplash.jpg ]
---

Senior software engineer at [Tecplot, Inc.](https://www.tecplot.com) and lead developer
of [PyTecplot](https://www.tecplot.com/products/pytecplot/).
[![Iso-surfaces of airspeed near a wing created using PyTecplot](img/wing_iso.png#floatright)](https://www.tecplot.com/docs/pytecplot/examples.html#mach-iso-surfaces) In my previous life, I was
a particle physicist and hold a PhD from UCLA on an analysis of data from the [CLAS
detector at Jefferson Lab, Virginia](https://www.jlab.org/Hall-B/). I enjoy complex
data, reproducible results and programming close to the metal. I also play lead
trombone for the [85th St Big Band](https://www.85thstreetbigband.com) in Seattle,
Washington.

# Physics Research

I was involved with the CLAS Collaboration at Jeffersion Lab starting as an undergraduate at UCLA under Prof. Bernard Nefkens and Prof. John Price, now at California State Domiguez Hills. This was from 2004 to 2015 and concluded with a Post-doctoral position with Prof. Ken Hicks at Ohio University. The final analysis I performed can be found in [Physical Review C](https://doi.org/10.1103/PhysRevC.98.062201) and on [arxiv.org](https://arxiv.org/abs/1809.00074). The primary focus of my work as a physicist centered around:

* Fundamental symmetries of quark interactions including charge, parity and time reversal symmetry (CPT), isospin, G-parity, chirality and flavor
* Non-perturbative QCD, quark confinement and the origin of mass
* Photoproduction of light and strange baryons

For details, reports and more [click here.](/page/physics_research)
