---
_build: { list: never }
title: Physics Research Interests
#bigimg: [ src: img/johannes-schenk-8mj7KKvyMDk-unsplash.jpg ]
---
I was involved with the CLAS Collaboration at Jeffersion Lab starting as an undergraduate at UCLA under Prof. Bernard Nefkens and Prof. John Price, now at California State Domiguez Hills. This was from 2004 to 2015 and concluded with a Post-doctoral position with Prof. Ken Hicks at Ohio University. The final analysis I performed can be found in [Physical Review C](https://doi.org/10.1103/PhysRevC.98.062201) and on [arxiv.org](https://arxiv.org/abs/1809.00074). The primary focus of my work as a physicist centered around:

* Fundamental symmetries of quark interactions including charge, parity and time reversal symmetry (CPT), isospin, G-parity, chirality and flavor
* Non-perturbative QCD, quark confinement and the origin of mass
* Photoproduction of light and strange baryons

# PhD Dissertation and Related Projects

My dissertation, titled **Ξ Hyperon Photoproduction from Threshold to 5.4 GeV with the CEBAF Large Acceptance Spectrometer**, wa completed at [University of California Los Angeles](http://home.physics.ucla.edu) under [Prof. B.M.K. Nefkens](https://senate.universityofcalifornia.edu/in-memoriam/files/bernard-nefkens.html) on November 15, 2010.

* [Screen version, single spaced and no margins (e-reader friendly, 9 MB, pdf)](goetz_thesis.pdf)
* [Print version, single spaced (112 MB postscript)](https://docs.google.com/uc?export=download&confirm=no_antivirus&id=0B2rq5Lzsv8KRMzU2MTA3M2MtZDliZS00NmU2LWJlM2EtNGIzNmMyYWZjY2Uy)
* [Print version as filed Nov. 15, 2010, double spaced (112 MB postscript)](https://docs.google.com/uc?export=download&confirm=no_antivirus&id=0B2rq5Lzsv8KRZGZlZmMwZWYtN2ZhMi00ZGYzLTgwNzYtOWJjY2M1MTFiMTJh)
* [PhD Defense Presentation, Nov. 5, 2010 (5 MB, pdf)](goetz_phddefense.pdf)
* [PhD Oral Exam Presentation, Aug 31, 2007 (2 MB, pdf)](goetz_phdoral.pdf)

Other projects I was involved with at Jefferson Laboratory were all at Hall-B with the CLAS and CLAS12 detectors.

* [Xi Photoproduction with g12](https://clasweb.jlab.org/rungroups/g12/wiki/index.php/G12_Xi_Paper)
* [CLAS12 Software: Calibrations Database](https://clasweb.jlab.org/wiki/index.php/CLAS_Offline)
* CLAS12 Online EPICS Drivers Update

# Selected Publications & Presentations

* [J. T. Goetz. **Ξ Hyperon Photoproduction from Threshold to 5.4 GeV with the CEBAF Large Acceptance Spectrometer**. PhD thesis, University of California Los Angeles, 2010](goetz_thesis.pdf)
* [J. T. Goetz. **Cascade Spectroscopy and Recent Photo-production Results from CLAS**. December 2009. Hadron 2009 Conference, Florida State Univ. Presentation Id: 114](https://doi.org/10.1063/1.3483392)
* [J. W. Price et al. **Exclusive photoproduction of the cascade (Ξ) hyperons**. Phys. Rev. C, 71:058201, 2005](http://link.aps.org/doi/10.1103/PhysRevC.71.058201)
* [J. T. Goetz et al. **Using controlled magnetic perturbations to study the plasma confinement in a spheromak**. Bull. Am. Phys. Soc., 48:150, 2003. Poster at APS meeting](https://ui.adsabs.harvard.edu/abs/2003APS..DPPGP1084G/abstract)
* [J. T. Goetz, M. Kennedy, and A. Ueda. **Precession and decay of μ leptons in copper**. Report for UCLA laboratory class 180F, June 2003](goetz_muprecession.pdf)
