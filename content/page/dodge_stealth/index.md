---
draft: true
---
# 1995 Dodge Stealth R/T

This is my 1995 Dodge Stealth R/T twin turbo V6 just after I installed brand new Enkei PF01's. The new wheels were 18x9 with a 45mm offset and I kept the stock tire size of 245/40. They clear the front strut cover by a long way and I might go with 255/40's when I eventually replace these tires. 265/40 would easily fit the rear wheels, but the differentials may not be able to cope with differing rubber contact between the front and back wheels.
