---
_build: { list: never }
title: Learning OpenGL
subtitle: Notes on learning modern OpenGL with Python
bigimg: [ src: img/behnam-mohsenzadeh-t9xJia7UJKQ-unsplash.jpg ]
---

This is a series of posts where I explore the beginnings of using modern OpenGL
with Python.

* [First Steps with OpenGL Using Python](/post/tech/opengl-with-python/01-opengl-with-python)
* [OpenGL with Python Part 2](/post/tech/opengl-with-python/02-opengl-with-python-pt2)

{{< relativelink "/post/tech/opengl-with-python/01-opengl-with-python" >}}
{{< relativelink "/post/tech/opengl-with-python/02-opengl-with-python-pt2" >}}

