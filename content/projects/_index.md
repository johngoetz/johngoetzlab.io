---
title: Projects
bigimg: [ src: img/jess-bailey-q10VITrVYUM-unsplash-crop.jpg ]
---

# [Learning OpenGL](opengl)

This is a series of posts where I explore the beginnings of using modern OpenGL
with Python.

* [First Steps with OpenGL Using Python](/post/tech/opengl-with-python/01-opengl-with-python)
* [OpenGL with Python Part 2](/post/tech/opengl-with-python/02-opengl-with-python-pt2)


# [Software Projects](public)

These are some of my publically accessible software projects.

* [Histogram Python Class](https://gitlab.com/johngoetz/histogram)
* [Colormaps Using CIELab Space](https://gitlab.com/johngoetz/colormap)
* [Harmonographs](https://gitlab.com/johngoetz/harmonograph)
* [Fuzzy Clock Gnome Extension](https://gitlab.com/johngoetz/FuzzyClock)
* [Wernher: Flight Control in Python for Kerbal Space Program](https://gitlab.com/johngoetz/wernher)

## GitLab Executors

* [LibVirt GitLab Executor](https://gitlab.com/johngoetz/gitlab-executor-libvirt)
* [Podman GitLab Executor](https://gitlab.com/johngoetz/gitlab-executor-podman)
* [UTM GitLab Executor]()
* [VMWare VCenter GitLab Executor]()