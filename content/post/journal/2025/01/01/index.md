---
title: New Year's Snowboarding
subtitle: Snoqualmie Alpental and Central
date: 2025-01-01
image: img/.png
bigimg: [ src: img/.png ]
---

From the cabin in Ronald, WA, we all went skiing on the mornings of New Year's Eve at Alpental and Day at Central. I thought there would more people on the Eve, but it turns out hardly anyone was there on the Eve and it was really crowded on the Day. I seem to recall the past two years where it wasn't crowded at all on New Years Day. It wasn't too bad though crowdwise. The snow was really good and there was a "bobsled" like track on Outback which was really fun.

Amelia and Peter switched boots and so Amelia tried snowboarding for the first time on the magic carpet and Peter managed pretty well on Amelia's too-long-for-him skis. She did very well too for the first time though she was not too happy about falling all the time. As for me, I was able to nagivate Triple-60 with thin cover just fine, but I'd really like to try to connect my turns through the moguls.

The drive from the cabin is easier than from Bellevue even though it's the same time. The amount of snow at Snoqualmie is thicker than last year's peak. Should be a great season this year.
