---
title: C++ Project Layout Design
draft: true
---

Starting a C++ project from scratch is generally very simple. A script that runs gcc on a single file and spits out an executable is usually sufficient at first. However, shortly after development has progressed, you start thinking about adding in unittests, examples, documentation, benchmarks, not to mention external dependencies in the way of libraries that are not neccessarily available on the system. Here, I'll try to document my C++ layout design preferences. It starts with the output from [cmake-init](https://github.com/friendlyanon/cmake-init) with the goals of supporting the following features:

1. Build the project with canonical `cmake` commands against libraries already available via `find_package()`.
2. Build with libraries installed via `conan install`.
3. Build with libraries from source as git submodules.
4. Optionally build unittests, benchmarks and examples.
5. Make some library features and associated external libraries completely optional.
6. Install resources (data files used at runtime).

I have long fought against cmake, mostly because of it's unclear scoping and state machine mechanisms, but it is the current standard. I used to prefer WAF and it looks like meson might be the forward-looking solution, however I'll do all this with cmake anyways.

# Top-level Project Directory Layout

The basic layout of a project which creates a single C++ library will be as follows:

```shell
.
├── docs
├── examples
├── external
├── resources
├── scripts
├── source
└── test
    ├── benchmarks
    └── unittests
```

Where all library source files, headers included, can be found under `source` since I find navigating to a different directory just to go from header to source file annoying. From here, I'll add in the neccessary source and cmake files to fill in the project and we'll end up with a nice template that can be used for future projects.

# Core Library Source

Let's add a source file and associated header which will expose a public function. Along with this, we'll create an example program to ensure we can use the library created. In this case, three cmake files are added:

```shell
./
├── CMakeLists.txt
├── examples/
│   ├── calc-sqrt.cpp
│   └── CMakeLists.txt
└── source/
    ├── calc/
    │   ├── calc.cpp
    │   └── calc.hpp
    └── CMakeLists.txt
```



# Library Config and Export Headers

# Examples

# Required External Libraries

# Optional Features and Optional Libraries

# Unittests and Benchmarks

# Linter and Formatter Scripts

# Resource Files

# Documentation

# CI/CD Configuration

# Conan Integration and Developmer Mode
