---
title: Practical Guide to Colormaps for 2D Data Visualization
draft: true
---

# References
* http://www.cet.edu.au/research-projects/geophysics-integrated-data-analysis/projects/colour-maps-with-uniform-perceptual-contrast
* arXiv:1509.03700 [cs.GR]
* http://medvis.org/2012/08/21/rainbow-colormaps-what-are-they-good-for-absolutely-nothing/
