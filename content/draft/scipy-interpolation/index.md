---
title: Multidimensional Fieldmap Interpolation Using SciPy
draft: true
---

Several examples can be found that deal with 1D and 2D fieldmaps, but few allow an arbitrary dimensional parameter space to be mapped to a scalar or vector.  interpolation scheme. Using scipy, I have created this small class which will act as a scalar field map with B-spline interpolation of order 1-5 (with dimensions=3 and B-spline order=1, the interpolation would be equivalent to "trilinear").

Fieldmaps are simply multivariate mappings from one space to another. They can be from any dimension to any other dimension. A common case is the scalar fieldmap in Euclidean space which is a mapping from 3D, typically *(x, y, z)*, to some single 1D value. A vector fieldmap on the same space would map *(x, y, z)* to an ND vector, say *(u, v, w)* if this was a 3D velocity vector.



The class is rather simple with only \_\_init\_\_() and \_\_call\_\_() implemented. It will store the results of the  `scipy.ndimage.spline_filter()` and use those as needed to get the  interpolated results with `scipy.ndimage.map_coordinates()`. So far I have  only tested it with a scalar values, but it should in-principle work as  a vector-valued field as well.

```python
import numpy as np
from scipy import ndimage

class FieldMap:
    '''
    a multidimensional scalar map on a regular grid
    with spline interpolation
    '''

    def __init__(self, grid, values, **kwargs):
        '''
        initialize with ogrid or numpy.ix_ object and
        the values on each point of the grid. I suppose
        you just have to make sure the grid spacing is
        a constant in each dimension. Typically, linspace()
        can be used:

            xx,yy = linspace(0,10,100), linspace(1,100,50)
            grid = np.ix_(xx,yy)
            zz = fn(*grid) # <-- need to define fn()
            fm = FieldMap(grid, zz)
            x,y = (1,2)
            z = fm(x,y)

        store grid object, initial point (x0,y0,z0...) and
        the delta (dx,dy,dz...) to the opposite corner of the
        first "nD-cube" (x1,y1,z1...)

        store the spline filter coefficients. Here, the user
        can specify the order of the interpolation
        as well as the storage data-type (float32, etc.)

        see ndimage.spline_filter() for details
        '''
        self.grid   = np.asarray(grid)
        self.p0     = np.array([np.ravel(x)[0] for x in self.grid])
        p1          = np.array([np.ravel(x)[1] for x in self.grid])
        self.inv_dp = 1. / (p1 - self.p0)

        self.mode = kwargs.pop('mode','constant')
        self.cval = kwargs.pop('cval',0.)

        self.coeffs = ndimage.spline_filter(values, **kwargs)

    def __call__(self, *pts, **kwargs):
        '''
        get interpolated value at a point or points.

        input must be a list of points
        (x0,x1...), (y0,y1...)...

        The points x,y,z... are converted to
        index-coordinates [(a,b,c...)...] which are then
        used to get the interpolated value using map_coordinates()

        mode='nearest' tells ndimage to use the closest
        point when pt lies outside the boundary of the grid.

        mode='constant' (with cval=0.0) returns 0. when
        pt lies outside the boundary of the grid.

        see ndimage.map_coordinates() for details
        '''
        points = []
        for pt in pts:
            if not hasattr(pt, '__iter__'):
                points += [[pt]]
            else:
                points += [pt]

        pts = np.asarray(points).T
        p = (pts - self.p0) * self.inv_dp

        ret = ndimage.map_coordinates(
            self.coeffs,
            p.T,
            prefilter=False,
            mode=kwargs.pop('mode',self.mode),
            cval=kwargs.pop('cval',self.cval),
            **kwargs )

        if len(ret) == 1:
            return ret[0]
        else:
            return ret

if __name__ == '__main__':
    from scipy import float32
    from numpy.testing import assert_array_almost_equal

    def fn(*pts):
        '''
        3D gaussian

        input can be a point x,y,z or points
        (x0,x1...), (y0,y1...)...
        '''
        x,y,z = np.asarray(pts)

        x0, sigx = 6., 1.5
        y0, sigy = 4., 1.0
        z0, sigz = 5., 3.0
        xx = ((x - x0)**2) / (2 * sigx**2)
        yy = ((y - y0)**2) / (2 * sigy**2)
        zz = ((z - z0)**2) / (2 * sigz**2)

        ret = np.exp(-(xx + yy + zz))

        return np.array(ret,dtype=float32)

    def fn_with_noise(*pts):

        x,y,z = np.asarray(pts)

        amp = 1
        noise_level = 0.0001
        noise = np.random.uniform(
            -noise_level*amp,
             noise_level*amp,
            x.shape )
        ret = amp * fn(x,y,z) + noise

        # do not allow this function to return negative values
        ret[ret<0.01] = 0

        return np.array(ret,dtype=float32)

    # setup the grid with 1M points
    g = np.linspace(0,10,101)
    grid_points = np.ix_(g,g,g)

    # get values of the map at grid points with noise
    values_on_grid = fn_with_noise(*grid_points)

    # setup the FieldMap object
    f = FieldMap(grid_points, values_on_grid, order=3, output=float32)

    # the (typically large) array values_on_grid can now be
    # safely deleted to free up memory. We can also delete
    # the grid. ogrid doesn't take up much space, but if an
    # mgrid object was used, then this will be worth while.
    del values_on_grid
    del grid_points

    # testing interpolation.
    # there should be a peak around (x,y,z) = (6,4,5)
    for x in np.linspace(0.5,8.5,3):
        for y in np.linspace(0.5,8.5,3):
            for z in np.linspace(0.5,8.5,3):


                pts = np.array([(x,y,z),(x+1,y+1,z+1)])
                xx,yy,zz = (x,x+1), (y,y+1), (z,z+1)
                a, b = f(xx,yy,zz), fn(xx,yy,zz)

                # set small numbers to 0 for readability of output
                a[a<0.01] = 0
                b[b<0.01] = 0

                print '(',x,y,z,')'
                try:
                    assert_array_almost_equal(a,b,decimal=4)
                    for aa,bb in zip(a,b):
                        print '    ',
                        print '{0: >7.5f} == {0: >7.5f}'.format(aa,bb)
                except AssertionError:
                    for aa,bb in zip(a,b):
                        print '    ',
                        print '{0: >7.5f} != {0: >7.5f}'.format(aa,bb)

    p = ([6],[4],[5])
    print p, f(*p), fn(*p)

    p = (6,4,5)
    print p, f(*p), fn(*p)

    x,y,z = p
    print p, f(x,y,z), fn(x,y,z)

    p = np.array([[6.0,4.0,5.0], [6.1,4.1,5.1]]).T
    x,y,z = p
    for pi, fpi, fnpi in zip(p.T, f(x,y,z), fn(x,y,z)):
        print pi, fpi, fnpi
```
