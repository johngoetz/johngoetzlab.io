---
title: Systems Provisioning in Code
draft: true
---

There are so many technologies that deal with development operations, systems deployment and computer infrastructure that it's sometimes hard to figure what they actually do or how they fit together and what are the limitations of each component. Worse yet, it's near impossible to determine the best tool for a specific job since many have overlapping capabilities. Here are the list of tools I'm talking about:

* [Vagrant by HashiCorp](https://www.vagrantup.com)
* [Packer by HashiCorp](https://www.packer.io)
* [Vault by HashiCorp](https://www.vaultproject.io)
* [Ansible](https://www.ansible.com)
* [Chef](https://www.chef.io)
* [Puppet](https://puppet.com)
* [SaltStack](https://saltstack.com)

There are some very useful sub-projects within those listed above which may warrent their own listing. For most of my work, I am interested in server configuration which means I don't need to push changes once the image is created, or even when the system is up. Most of the provisioners have a "local" version, but of course, they all use a different name for the same thing. Here are a few that stand out:

* [Docker Compose](https://docs.docker.com/compose)
* [Chef Solo](https://docs.chef.io/chef_solo.html)
* [Salt Masterless](https://docs.saltstack.com/en/latest/topics/tutorials/quickstart.html)

The projects above are designed around the philosophy that all infrastructure configuration should be written in code and stored in version control. They work with physical machines, virtual machines or containers. The major virtualization technologies in this scope are:

* [Linux Containers (LXC, LXD)](https://linuxcontainers.org)
* [Docker](https://www.docker.com)
* [Parallels](https://www.parallels.com)
* [KVM](www.linux-kvm.org)
* [Xen](https://www.xenproject.org)
* [QEMU](https://www.qemu.org)
* [VirtualBox by Oracle](https://www.virtualbox.org)
* [VMWare](https://www.vmware.com)
* [Hyper-V by Microsoft](https://en.wikipedia.org/wiki/Hyper-V)

Finally, there are tons of other projects in this field that will not be considered. These include remote administration and cloud operations or scaling:

* [Terraform by HashiCorp](https://www.terraform.io)
* [Habitat by Chef](https://www.habitat.sh)
* [Kubernetes](https://kubernetes.io)
* [Mesos by Apache](http://mesos.apache.org)
* [Converge](http://converge.aster.is)

# Machine Configuration Management

The goal of putting systems configuration in code is to *completely describe* the machines needed and to create and deploy them easily - or at least consistently. I am interested in creating several operating systems - various distributions of Linux, MacOS and Windows - in different configurations - containers, virtual machines with virtual GPU, virtual machines with direct access to a GPU, and physical machines.

Containers will start from trusted official images such as CentOS's Docker images. For Docker images, a Dockerfile may be sufficient to build the image, but it may be that we can use something like [Packer](https://www.packer.io) to merge configurations into a single tool. Virtual and physical machines will all start with the official base images. The setup for these will need a kickstart file for an unattended installation. After or during the installation, a local user account must be created, some bare-minimum applications installed and the image will be saved off. Thereafter, other images of varying purpose should be created, all of which should be easily deployed.

## Headless Linux as Containers

Without going into the list of advantages that containers provide for headless Linux systems, I'm just going to assert that we'll want containers for non-GUI systems which will include servers, build machines and batch-level test machines. Most linux distributions already provide a Docker image and that's where we'll start. From the base Docker image, a series of Dockerfiles could be used to create the Docker images that will be called "base," "build" and "test."

Linux Containers (LXC/LXD) provide a theoretical performance advantage over Docker as well as behaving more like a real installation of Linux - systemd is typically shutdown in a Docker container, not so with LXC. Another seeming advantage of LXC is that user IDs are mapped to the host so that the root user (id = 0) inside the container is some other unpriviledged user (id >= 500) on the host. This complicates some operations like mounting, but simplifies others such as transferring files to and from the container. Finally, LXC offers device pass-through such as GPUs which may allow for graphics testing using containers - something extremely hard (maybe impossible?) to setup using Docker.

Despite the apparent benefits of Linux Containers, our first investigation will be with Docker since it is supported directly by GitLab's Continuous Integration system, and because most Linux distributions we are using have official Docker images. For all the security mindedness of Linux Containers, I did not find a repository of sufficiently certified pre-built images.

## Virtual and Physical Machines

Since we are using GitLab for our CI, it makes sense to start looking into VirtualBox for which GitLab already has a runner type. As far as creating images from base ISO distributions of operating systems, it looks like [Packer](https://www.packer.io) will be the tool to use. I'm not even sure there is an alternative beyond a bunch of shell scripts or a part-time intern. Physical machines present a slight problem with a possible solution: the VM images might be converted to ISO images which can then just be written directly to the physical disks. This would be nice if the operating system picked up on the hardware change and adjusted accordingly.

# Using Packer to Provision Multiple Docker and VirtualBox Linux Images

The first investigation into systems configuration in code resulted in setting up [Packer](https://www.packer.io) configurations for CentOS 6 running in Docker and VirtualBox. I wanted to control the minor version of the system so we can test on, say version 6.5 even though 6.10 was the newest, and started to have a configuration file for each minor version but found it to be uneccessary in the end. For both Docker and VirtualBox, we create these images:

* base
* build
* test
* develop

The "base" image is the minimal setup with a local user account and network configuration designed for our needs. The "build" and "test" images derive from "base" and contain only enough to build or test (all) of our products. The VMs will contain a desktop such as Gnome Shell and as such will be able to test graphics while the containers will only be able to run or test the products in headless/batch mode. Finally, the "develop" image will have a full-blown development environment installed - this is essentially everything that "build" and "test" have plus an IDE.

