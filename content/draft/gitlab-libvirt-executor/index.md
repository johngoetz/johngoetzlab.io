---
draft: true
type: post
title: LibVirt Executor for the GitLab CI System
subtitle: Managing QEMU/KVM virtual machines as runners
publishDate: 2022-11-03
#image: img/shubham-bombarde-vRSMuso1pXw-unsplash-crop.jpg
#bigimg: [ src: img/shubham-bombarde-vRSMuso1pXw-unsplash.jpg ]
---

https://docs.gitlab.com/runner/executors/custom.html
https://docs.gitlab.com/runner/executors/custom_examples/libvirt.html

https://github.com/libvirt/libvirt-gitlab-executor
https://github.com/andreineustroev/gitlab-vagrant-executor

https://gitlab.com/libvirt/libvirt-gitlab-executor

<!--more-->
