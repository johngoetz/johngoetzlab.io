---
draft: true
---
# Wigner D Function
posted Jun 7, 2012, 6:17 AM by John Goetz   [ updated Jun 18, 2012, 6:35 AM ]
The Wigner D-function is the rotation operator in the spin (jm) representation. I couldn't find a fast implementation of it in scipy (nor in several other languages/packages except Mathematica and maybe ROOT) so I wrote the following. It uses the spherical harmonics and Legendre polynomials for certain values of j,m,n. It's vectorized where it counts and so is fast enough for me. Additionally, for j >> m,n (actually for j > m+10 and j > n+10 by default) I approximate it with Bessel functions. This avoids potential problems with higher order evaluations of the Jacobi polynomials. At any rate, it can produce some interesting pictures. The python module wignerd.py is attached and the plots shown were produced in the example "main" part of the script.
