---
title: Getting Started with VulkanSceneGraph on Fedora 38
draft: true
---

Following https://vsg-dev.github.io/vsgTutorial/SettingTheScene/BuildingVulkanSceneGraph, here are the steps I took to compile the VSG examples:

```
dnf install git g++ cmake
dnf install vulkan-headers vulkan-tools vulkan-validation-layers-devel
dnf install assimp-devel freetype-devel openexr-devel
```

I also installed these because they looked interesting:

```
dnf install mangohud vkBasalt vkmark vkroots-devel
```

In a directory `~/workspace/vsg`, I created a file `enable`:

```
#!/bin/bash

HERE=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

export INSTALL_DIR=$HERE
export PROJECT_DIR=$HERE/src
export PATH=${PATH}:${INSTALL_DIR}/bin
export LD_LIBRARY_PATH=${D_LIBRARY_PATH}:${INSTALL_DIR}/lib

export VSG_FILE_PATH=${PROJECT_DIR}/vsgExamples/data
export VSG_FILE_CACHE=${PROJECT_DIR}/vsgFileCache
```

Then I followed along with cloning the various repositories after sourcing this file:

```
source ~/vsg/enable
mkdir ${PROJECT_DIR}
cd ${PROJECT_DIR}

git clone https://github.com/vsg-dev/VulkanSceneGraph.git
git clone https://github.com/vsg-dev/vsgXchange.git
git clone https://github.com/vsg-dev/vsgExamples.git
git clone https://github.com/vsg-dev/vsgTutorial.git
```
