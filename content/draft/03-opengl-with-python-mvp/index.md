---
title: 'OpenGL with Python Part 3: The Model-View-Projection Matrix'
draft: true
keywords: programming opengl python shaders glfw
---

Adding detail and sophistication to the model-view-projection matrix for managing camera movement within an OpenGL context.

<!--more-->

In the [previous post](../02-opengl-with-python-pt2), a couple matrices were created and multiplied to create a model-view-projection matrix which was pushed to the shaders using a uniform variable. Expanding on this concept, these matrices will be elevated to a set of classes in Python. Around these classes, we will build up a set of movement controls connected to keyboard and mouse inputs. It's all just a bit of linear algebra fun!

## Model Matrix

The model matrix consists of a uniform scaling followed by a rotation and translation. I'm not too interested in warping the model but this is where that kind of thing would go. In the [previous post](../opengl-with-python-pt2), this was just the identity matrix which meant the model vertices were not moved off the origin in any way.

The following class uses the [numpy-quaternion](https://pypi.org/project/numpy-quaternion) Python package along with [numpy](https://docs.scipy.org/doc) to handle most of the harder math - though it's not too difficult at this point. Also, we put a small amount of effort to optimize this object such that it doesn't perform the heavier calculations if it doesn't have to. Specifically, it caches the translation and rotation matrices as well as the final model transformation matrix. I confess that I did not profile the code to see if this optimization was needed or effective - it seemed like a cheap way to make an attempt at semi-performant code.

```python
import numpy as np
import quaternion

class ModelTransform:
    def __init__(self):
        self._scale = np.full(3, 1)
        self._position = np.zeros(3)
        self._rotation = np.quaternion(1, 0, 0, 0)
        self._matrix = None
        self._translation_matrix = None
        self._rotation_matrix = None

    @property
    def scale(self):
        return self._scale

    @scale.setter
    def scale(self, value):
        self._scale = np.asarray(value)
        self._matrix, self._translation_matrix = None, None

    @property
    def position(self):
        return self._position

    @position.setter
    def position(self, value):
        self._position = np.asarray(value)
        self._matrix, self._translation_matrix = None, None

    @property
    def rotation(self):
        return self._rotation

    @rotation.setter
    def rotation(self, value):
        self._rotation = np.quaternion(value)
        self._matrix, self._rotation_matrix = None, None

    @property
    def matrix(self):
        if self._matrix is None:
            if self._translation_matrix is None:
                sx, sy, sz = self._scale
                x, y, z = self._position
                self._translation_matrix = np.array((
                    (sx,  0,  0, 0),
                    ( 0, sy,  0, 0),
                    ( 0,  0, sz, 0),
                    ( x,  y,  z, 1)))
            if self._rotation_matrix is None:
                self._rotation_matrix = np.identity(4)
                self._rotation_matrix[:3,:3] = \
                    quaternion.as_rotation_matrix(self._rotation)
            R = self._rotation_matrix
            T = self._translation_matrix
            self._matrix = R @ T
        return self._matrix
```

As you can see when one of the three properties `scale`, `position` and `rotation` are set, the final matrix and the translation or rotation matrices are invalidated by being set to `None`. The `matrix` property is in charge of recalculating the intermediate matrices and the final transformation if hasn't done so already or if it has been invalidated.

The rotation is represented as a quaternion. Here are some references to read up on for those curious about the math:

* [numpy-quaternion github page](https://github.com/moble/quaternion)
* [numpy-quaternion docs](https://quaternion.readthedocs.io/en/latest/)
* [opengl-tutorial page on quaternions](https://www.opengl-tutorial.org/intermediate-tutorials/tutorial-17-quaternions/)
* [quaterion wiki page](https://en.wikipedia.org/wiki/Quaternion)

The scaling and transformation matrix is constructed by the components as seen above and the final model transformation is then calculated by multiplying this by the rotation matrix representation of the quaternion.

## Projection Matrix

There are two projections

## View Matrix

## Movements and Camera Control

## Cubes and Triangles
