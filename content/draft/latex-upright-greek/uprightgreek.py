from matplotlib import rc, pyplot

rc('text', usetex=True)
rc('text.latex', preamble = ','.join('''
\usepackage{txfonts}
\usepackage{lmodern}
'''.split()))
rc('font', family='serif', weight='normal', style='normal')

if __name__ == '__main__':
    fig = pyplot.figure(figsize=(4,2))
    ax = fig.add_subplot(1,1,1)
    ax.xaxis.set_visible(False)
    ax.yaxis.set_visible(False)

    gchars = ''' alpha beta gamma delta epsilon zeta eta theta iota
    kappa lambda mu nu xi pi rho sigma tau epsilon phi chi psi omega
    '''.split()

    ax.text(0.05,0.72, r'upright greek in math mode:')
    ax.text(0.1,0.6, '$'+ ' '.join(['\\'+c+'up' for c in gchars]) +'$')
    ax.text(0.05,0.42, r'regular greek in math mode:')
    ax.text(0.1,0.3, '$'+ ' '.join(['\\'+c for c in gchars]) +'$')

    pyplot.show()
