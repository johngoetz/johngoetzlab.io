---
draft: true
---
# upright greek characters in LaTeX and matplotlib
posted Aug 3, 2011, 8:16 AM by John Goetz   [ updated Jan 16, 2014, 10:31 AM ]
Upright greek characters can be incorporated into any LaTeX document without modifying other fonts with the following statements in the tex file preamble:
uprightgreek.tex

\usepackage{txfonts}
\usepackage{lmodern}

This same set of packages can be brought into matplotlib with the following calls to matplotlib.rc:
