from matplotlib import rc, pyplot
import numpy, scipy
from numpy import random, histogram
from scipy import linspace, optimize

rc('legend', fontsize=8)

### bounding functions
# lower bound:            ubound(boundary_value, parameter)
# upper bound:            ubound(boundary_value, parameter)
# lower and upper bounds: bound([low, high], parameter)
# fixed parameter:        fixed(fixed_value, parameter)
lbound = lambda p, x: 1e4*numpy.sqrt(p-x) + 1e-3*(p-x) if (x<p) else 0
ubound = lambda p, x: 1e4*numpy.sqrt(x-p) + 1e-3*(x-p) if (x>p) else 0
bound  = lambda p, x: lbound(p[0],x) + ubound(p[1],x)
fixed  = lambda p, x: bound((p,p), x)

# this is the function we want to fit to the data
# gaus(mean, sigma, amplitude)
gaus = lambda p, x: p[2] * scipy.exp(-(x - p[0])**2 / (2. * p[1]**2))

# generate some data (a gaussian distribution with mean=0, sigma=3)
xx = linspace(-10,10,200)
ydata,_ = histogram(random.normal(0,3,10000), bins=200, range=(-10,10))

# set initial parameters
pinit = [1,1,1]

# unbounded function example
errfn = lambda p, x, y: gaus(p,x) - y
pfit,success = optimize.leastsq(errfn, pinit, args=(xx, ydata))
yfit = gaus(pfit,xx)

# set the amplitude parameter's upper bound to be 90
errfn = lambda p, x, y: gaus(p,x) - y + ubound(90,p[2])
pfit,success = optimize.leastsq(errfn, pinit, args=(xx, ydata))
yfit_lim1 = gaus(pfit,xx)

# set a boundary on the mean to be [2,3]
errfn = lambda p, x, y: gaus(p,x) - y + bound([2,3],p[0])
pfit,success = optimize.leastsq(errfn, pinit, args=(xx, ydata))
yfit_lim2 = gaus(pfit,xx)

# plot the results
fig = pyplot.figure(figsize=(6,4))
ax = fig.add_subplot(1,1,1)
ax.plot(xx,ydata, marker='+', linestyle='none', label=r'data')
ax.plot(xx,yfit, color='green', label=r'unbounded')
ax.plot(xx,yfit_lim1, color='red', label=r'bound amplitude')
ax.plot(xx,yfit_lim2, color='orange', label=r'bound mean')
ax.legend(numpoints=1, loc='upper left')

pyplot.show()
