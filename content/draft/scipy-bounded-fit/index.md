---
draft: true
---
# setting bounded and fixed parameters in scipy fitting routines
posted Aug 4, 2011, 6:02 AM by John Goetz   [ updated Aug 16, 2011, 4:28 PM ]
It seems that python's scipy package does not have built-in support for bounding parameters in a fit. I usually work around this by making the error function blow up above or below a certain point. But there are some subtleties involved that make this solution non-trivial to implement.

Below, I show two functions which act as the lower and upper boundaries of a variable x at a point p: ubound(p,x) and lbound(p,x). They are discontinuous at p and increase monotonically the farther away x is from p. When x is very far from p, the slope of the function is dominated by |x-p| so that the fitter can get a reasonable derivative and know where to step next.

Previous attempts at this method using functions that were essentially infinity above the point p resulted in the fitter not knowing how to get back to the region of interest. This does mean that the parameter could end up above an upper bound or below a lower bound, however the tolerance can be adjusted with the large constant factor in the functions (1e4 in the example below).

One caveat is that the scipy.optimize.leastsq() method requires reasonable initial parameters and sometimes it fails the fit. A more robust method might be to calculate the mean, standard deviation and maximum of the data and set these as the initial parameters for the mean, sigma and amplitude respectively. The scipy.odr minimization package (a wrapper around the FORTRAN ODRPACK which stands for Orthogonal Distance Regression) provides for defining a function that dynamically calculates the initial parameters and is a bit more general in fitting data where the error in y varies.
