---
title: Compute Engine using Vulkan
draft: true
---

Starting from Neil Henning's excellent post (http://www.duskborn.com/a-simple-vulkan-compute-example/) where he introduces a "minimal" compute engine using Vulkan, I set out to understand, extend and then compare the performance of using this technology to push general array calculations on to a GPU.
Getting Up and Running
The first step was to get Hennig's example to run. The biggest hurdle for me was that I had to realize what this was all about (see the original post for the full version of this bit of code):

```
    enum {
      RESERVED_ID = 0,
      FUNC_ID,
      IN_ID,
      OUT_ID,
 [... snip ...]
    };

    enum {
      INPUT = 1,
      UNIFORM = 2,
      BUFFER_BLOCK = 3,
      ARRAY_STRIDE = 6,
 [... snip ...]
    };

    int32_t shader[] = {
      // first is the SPIR-V header
      0x07230203, // magic header ID
      0x00010000, // version 1.0.0
      0,          // generator (optional)
      BOUND,      // bound
      0,          // schema

      // OpCapability Shader
      (2 << 16) | OP_CAPABILITY, 1,

      // OpMemoryModel Logical Simple
      (3 << 16) | OP_MEMORY_MODEL, 0, 0,

      // OpEntryPoint GLCompute %FUNC_ID "f" %IN_ID %OUT_ID
      (4 << 16) | OP_ENTRY_POINT, 5, FUNC_ID, 0x00000066,

      // OpExecutionMode %FUNC_ID LocalSize 1 1 1
      (6 << 16) | OP_EXECUTION_MODE, FUNC_ID, 17, 1, 1, 1,

      // next declare decorations
      (3 << 16) | OP_DECORATE, STRUCT_ID, BUFFER_BLOCK,
      (4 << 16) | OP_DECORATE, GLOBAL_INVOCATION_ID, BUILTIN, GLOBAL_INVOCATION,
      (4 << 16) | OP_DECORATE, IN_ID, DESCRIPTOR_SET, 0,
      (4 << 16) | OP_DECORATE, IN_ID, BINDING, 0,
      (4 << 16) | OP_DECORATE, OUT_ID, DESCRIPTOR_SET, 0,
      (4 << 16) | OP_DECORATE, OUT_ID, BINDING, 1,
 [... snip ...]
    };
```

Yikes!!! I am no shader expert and I suppose knowing how to produce this kind of code might be useful, but I really wanted to have the shader code a bit more readable. Luckily, in the comments of the mentioned post, some kind soul disassembled the above shader into GLSL and obtained something like this:

```
#version 430
layout(local_size_x = 1, local_size_y = 1, local_size_z = 1) in;
layout(binding = 0, std430) buffer _9 {
  int data[16384];
} inArray;
layout(binding = 1, std430) buffer _9 {
  int data[16384];
} outArray;
void main() {
  outArray.data[gl_GlobalInvocationID.x] = inArray.data[gl_GlobalInvocationID.x];
}
```

Much better, but there's a problem: this won't compile and the code using it must be adapted to call main() instead of the original f(). We'll first fix the shader by making one small change. The shader Layout identifiers must be unique so we change the two _9's to a and b.

```
#version 430
layout(local_size_x=1, local_size_y=1, local_size_z=1) in;

layout(binding=0, std430) buffer a {
  int data[16384];
} inArray;

layout(binding=1, std430) buffer b {
  int data[16384];
} outArray;

void main() {
  int i = gl_GlobalInvocationID.x;
  outArray.data[i] = inArray.data[i];
}
```

Great. So now we need to build the shader binary with a command like glslangValidator -V -oshader/memcopy.spv shader/memcopy.comp, and pull this into the Vulkan shader module:

```
std::unique_ptr<uint32_t[]> shader;
size_t shaderSize;
{
  std::ifstream file("shaders/memcopy.spv",
                     std::ios::ate | std::ios::binary);
  if (!file.is_open()) {
    throw std::runtime_error("failed to open file!");
  }
  shaderSize = file.tellg();
  file.seekg(0);
  shader = std::make_unique<uint32_t[]>(shaderSize / sizeof(uint32_t));
  file.read(reinterpret_cast<char*>(shader.get()), shaderSize);
}

VkShaderModuleCreateInfo shaderModuleCreateInfo = {
  VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
  0,
  0,
  shaderSize,
  shader.get()
};
```
