---
draft: true
---
# matplotlib colormap adjustments
posted Feb 18, 2010, 10:04 PM by John Goetz   [ updated Aug 4, 2011, 6:00 AM ]
When plotting a two dimensional histogram, the color maps provided by matplotlib need some adjustment to bring out features in the data. Here is a bit of code that eases the creation of new color maps with constrast in the region you want:
