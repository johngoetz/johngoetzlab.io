---
draft: true
---
# fitting a profile in 2D histogram data
posted Jan 30, 2012, 10:26 AM by John Goetz   [ updated Jan 30, 2012, 10:27 AM ]
Here I fit a 2D histogram using a single function which takes (x,y) and returns z. The function is a Gaussian in y. The parameters of the Gaussian (amplitude, mean and sigma) are each a polynomial in x. As it is written below, one can change the order of each of the polynomials independently. The initial guess for the parameters is determined using the projection of the data onto the y axis which only works if the model is accurate enough.
