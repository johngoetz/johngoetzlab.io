---
draft: true
---
# Notes

These are my notes about data analysis using the numpy, scipy and matplotlib packages in python. My focus is on high-energy particle research, but I am deeply fascinated by the whole subject of data presentation and I may digress. The basic problems I will address here include handling large multidimensional datasets and creating eye-catching, information-rich graphics. Most of my work is done using the python programming language with the packages: numpy and scipy for handling basic arrays and minimization routines, matplotlib for graphics (check out their excellent gallery page), and h5py and pytables for data storage in files. The last two are python wrappers around the hdf5 serialization library though pytables provides optimized functionality for making selections in ntuples and arrays.

## Multidimensional field map interpolation using scipy

There are many examples out there that deal with 1 and 2D field maps, but relatively few that allow an arbitrary dimensional parameter space to be mapped to a scalar or better yet, a vector interpolation scheme. Using scipy, I have created this small class which will act as a scalar field map with B-spline interpolation of order 1-5 (with dimensions=3 and B-spline order=1, the interpolation would be equivalent to "trilinear").

The class is rather simple with only __init__() and __call__() implemented. It will store the results of the scipy.ndimage.spline_filter() and use those as needed to get the interpolated results with scipy.ndimage.map_coordinates(). So far I have only tested it with a scalar values, but it should in-principle work as a vector-valued field as well.

