## Articles

* [Technology Blog Posts](/post/tech)
* [Personal Journal](/post/journal)

## Public Projects

* [LibVirt GitLab Executor](https://gitlab.com/johngoetz/gitlab-executor-libvirt)
* [Podman GitLab Executor](https://gitlab.com/johngoetz/gitlab-executor-podman)
* [Histogram Python Class](https://gitlab.com/johngoetz/histogram)
* [Colormaps Using CIELab Space](https://gitlab.com/johngoetz/colormap)
* [Harmonographs](https://gitlab.com/johngoetz/harmonograph)
* [Fuzzy Clock Gnome Extension](https://gitlab.com/johngoetz/FuzzyClock)
* [Wernher: Flight Control in Python for Kerbal Space Program](https://gitlab.com/johngoetz/wernher)
